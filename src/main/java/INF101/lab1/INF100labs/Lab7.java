package INF101.lab1.INF100labs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;


/**
 * Implement the methods removeRow and allRowsAndColsAreEqualSum.
 * These programming tasks was part of lab7 in INF100 fall 2022. You can find
 * them here: https://inf100.ii.uib.no/lab/7/
 */
public class Lab7 {

    public static void main(String[] args) {
        ArrayList<ArrayList<Integer>> grid1 = new ArrayList<>();
        grid1.add(new ArrayList<>(Arrays.asList(11, 12, 13)));
        grid1.add(new ArrayList<>(Arrays.asList(21, 22, 23)));
        grid1.add(new ArrayList<>(Arrays.asList(31, 32, 33)));
        removeRow(grid1, 0);
        for (ArrayList<Integer> row : grid1) {
            System.out.println(row);
        }

        ArrayList<ArrayList<Integer>> grid2 = new ArrayList<>();
        grid2.add(new ArrayList<>(Arrays.asList(4, 4, 4)));
        grid2.add(new ArrayList<>(Arrays.asList(4, 4, 4)));
        grid2.add(new ArrayList<>(Arrays.asList(4, 4, 4)));
        System.out.println(allRowsAndColsAreEqualSum(grid2)); 
    }

    public static void removeRow(ArrayList<ArrayList<Integer>> grid, int row) {
        grid.remove(row);
    }

    public static boolean allRowsAndColsAreEqualSum(ArrayList<ArrayList<Integer>> grid) {
        HashSet<Integer> rowSums = new HashSet<>();
        HashSet<Integer> colSums = new HashSet<>();
        for (ArrayList<Integer> row : grid) {
            rowSums.add(row.stream().mapToInt(Integer::intValue).sum());
        }
        for (int j = 0; j < grid.get(0).size(); j++) {
            int colSum = 0;
            for (ArrayList<Integer> row : grid) {
                colSum += row.get(j);
            }
            colSums.add(colSum);
        }
        return rowSums.size() == 1 && colSums.size() == 1 && rowSums.equals(colSums);
    }
}

    