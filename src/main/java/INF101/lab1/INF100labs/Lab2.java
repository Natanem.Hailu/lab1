package INF101.lab1.INF100labs;

/**
 * Implement the methods findLongestWords, isLeapYear and isEvenPositiveInt.
 * These programming tasks was part of lab2 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/2/
 */
public class Lab2 {
    
    public static void main(String[] args) {
        findLongestWords("Game", "Action", "Champion");
        System.out.println(isLeapYear(2020));
        System.out.println(isEvenPositiveInt(42));
    }

    public static void findLongestWords(String word1, String word2, String word3) {
        int maxLength = Math.max(word1.length(), Math.max(word2.length(), word3.length()));
        if (word1.length() == maxLength) System.out.println(word1);
        if (word2.length() == maxLength) System.out.println(word2);
        if (word3.length() == maxLength) System.out.println(word3);
    }

    public static boolean isLeapYear(int year) {
        if ((year % 4 == 0 && year % 100 != 0) || (year % 100 == 0 && year % 400  == 0)) {
            return true;
        } else {         
            return false; 
        }
    }

    public static boolean isEvenPositiveInt(int num) {
        if (num > 0 && num % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }
}
