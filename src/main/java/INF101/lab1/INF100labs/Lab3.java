package INF101.lab1.INF100labs;

/**
 * Implement the methods multiplesOfSevenUpTo, multiplicationTable and crossSum.
 * These programming tasks was part of lab3 in INF100 fall 2022. You can find them here: https://inf100.ii.uib.no/lab/3/
 */
public class Lab3 {
    
    public static void main(String[] args) {
        multiplesOfSevenUpTo(49);
        System.out.println("---");
        multiplicationTable(5);
        System.out.println("---");
        System.out.println(crossSum(1234));
    }

    public static void multiplesOfSevenUpTo(int n) {
        int counter = 0;
        while (counter <= n) {
            if (counter % 7 == 0 && counter > 0) {
                System.out.println(counter);
            }
            counter++;
        }
    }

    public static void multiplicationTable(int n) {
        for (int i = 1; i <= n; i++) {
            System.out.print(i + ": ");
            for (int j = 1; j <= n; j++) {
                System.out.print(i * j + " ");
            }
            System.out.println();
        }
    }

    public static int crossSum(int num) {
        int sum = 0;
        String number = Integer.toString(num);
        for (int i = 0; i < number.length(); i++) {
            sum += Character.getNumericValue(number.charAt(i));
        }
        return sum;
    }
}